﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Elight.Utility.Extension;
using Elight.Utility.Files;
using UEditorNetCore;
using Microsoft.AspNetCore.Mvc;

namespace Elight.WebUI
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddUEditorService();
            //使用缓存
            services.AddMemoryCache();
            services.AddMvc(options => { options.EnableEndpointRouting = false; });
            //services.AddMvc(options => { options.EnableEndpointRouting = false; })
            //.SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            //    .AddNewtonsoftJson(options =>
            //    {
            //        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            //        options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm";
            //    });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        }

        public void Configure(IApplicationBuilder app, IServiceProvider svp)
        {

            MyHttpContext.ServiceProvider = app.ApplicationServices;
            //启用Session
            app.UseSession();

            app.UseExceptionHandler("/Index/Error");

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "default",
                   template: "{controller=Index}/{action=Index}/{id?}");

                routes.MapRoute(
                  name: "System",
                  template: "{area:exists}/{controller=Index}/{action=Index}/{id?}");
            });
        }
    }
}
