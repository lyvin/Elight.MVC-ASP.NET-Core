﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Elight.Utility.Extension;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.Memory;

namespace Elight.Utility.Web
{

    public static class WebHelper
    {
        #region Session操作
        /// <summary>
        /// 设置Session。
        /// </summary>
        /// <typeparam name="T">键值类型</typeparam>
        /// <param name="key">键名</param>
        /// <param name="value">键值</param>
        public static void SetSession<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) :
            JsonConvert.DeserializeObject<T>(value);
        }
        #endregion
    }
}
