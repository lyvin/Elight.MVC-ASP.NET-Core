﻿using Elight.Utility.Files;
using System;

namespace Elight.Utility
{
    /// <summary>
    /// 全局配置文件
    /// </summary>
    public class ConstUtils
    {
        public static readonly string SoftwareName = ConfigurationManager.AppSettings["SoftwareName"];
    }
}
